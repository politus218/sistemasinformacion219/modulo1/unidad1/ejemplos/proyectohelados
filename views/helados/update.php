<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Helados */

$this->title = 'Update Helados: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Helados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="helados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
